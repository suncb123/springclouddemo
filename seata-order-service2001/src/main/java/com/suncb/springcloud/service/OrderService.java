package com.suncb.springcloud.service;

import com.suncb.springcloud.domain.Order;

public interface OrderService {
    /**
     * 创建订单
     */
    void create(Order order);

}
