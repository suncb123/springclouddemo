package com.suncb.springcloud.controller;

import com.suncb.springcloud.entity.CommonBoy;
import com.suncb.springcloud.entity.PayMentEntity;
import com.suncb.springcloud.service.IPayMentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * Description payment com.suncb.springcloud.controller
 *
 * @Author suncb
 * @Date 2021/6/22 10:36
 */
@RestController
@RequestMapping(value = "payMentController")
public class PayMentController {
    @Autowired
    private IPayMentService payMentService;
    @Value("${server.port}")
    private String port;

    @GetMapping(value = "/queryPayMent/{id}")
    public CommonBoy<PayMentEntity> queryPayMent(@PathVariable("id") Integer id){
        PayMentEntity payMentEntity = payMentService.queryPayMent(id);
        if(payMentEntity != null){
            return new CommonBoy(200,"查询成功,端口:" + port,payMentEntity);
        }else {
            return new CommonBoy(404,"未查询到结果",null);
        }
    }
    @PostMapping(value = "/addPayMent")
    public CommonBoy addPayMent(@RequestBody PayMentEntity payMentEntity){
        Integer result = payMentService.addPayMent(payMentEntity);
        if(result > 0){
            return new CommonBoy(200,"插入成功",payMentEntity);
        }else {
            return new CommonBoy(404,"插入失败",null);
        }
    }
    @GetMapping(value = "/payment/lb")
    public String getPaymentLB()
    {
        return port;
    }

    @GetMapping(value = "/payment/timeout")
    public String getPaymentTimeOut()
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return port;
    }
}
