package com.suncb.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * Description payment 启动类
 *
 * @Author suncb
 * @Date 2021/6/21 21:00
 */
@SpringBootApplication
@EnableEurekaClient
public class PayMent8002 {
    public static void main(String[] args) {
        SpringApplication.run(PayMent8002.class,args);
    }
}
