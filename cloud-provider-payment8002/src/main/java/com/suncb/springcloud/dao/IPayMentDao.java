package com.suncb.springcloud.dao;

import com.suncb.springcloud.entity.PayMentEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface IPayMentDao {
    PayMentEntity queryPayMent(@Param("id") Integer id);
    Integer addPayMent(PayMentEntity payMentEntity);
}
