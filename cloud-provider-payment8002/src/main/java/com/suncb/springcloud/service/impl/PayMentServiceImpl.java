package com.suncb.springcloud.service.impl;

import com.suncb.springcloud.dao.IPayMentDao;
import com.suncb.springcloud.entity.PayMentEntity;
import com.suncb.springcloud.service.IPayMentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayMentServiceImpl implements IPayMentService {
    @Autowired
    private IPayMentDao payMentDao;
    @Override
    public PayMentEntity queryPayMent(Integer id) {
        return payMentDao.queryPayMent(id);
    }

    @Override
    public Integer addPayMent(PayMentEntity payMentEntity) {
        return payMentDao.addPayMent(payMentEntity);
    }
}
