package com.suncb.springcloud.service;

import com.suncb.springcloud.entity.PayMentEntity;

public interface IPayMentService {
    PayMentEntity queryPayMent(Integer id);
    Integer addPayMent(PayMentEntity payMentEntity);
}
