package com.suncb.springcloud.controller;

import com.suncb.springcloud.entity.CommonBoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "customerzk")
public class OrderController {
    private static final String PAYMENT_URL = "http://cloud-provider-payment";
   // private static final String PAYMENT_URL = "http://localhost:8004";
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(value = "/payment")
    public String paymentInfo(){
        String result = restTemplate.getForObject(PAYMENT_URL + "/paymentCon/payment/zk",String.class);
        System.out.println("zookeeper消费者:" + result);
        return result;
    }
}
