package com.suncb.springcloud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonBoy<T> {
    private Integer code;
    private String msg;
    private T data;
    public CommonBoy(Integer code, String msg){
        this(code,msg,null);
    }
}
