package com.suncb.springcloud.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.netflix.ribbon.proxy.annotation.Hystrix;
import com.suncb.springcloud.service.IPayMentHystrixService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("consumer")
@DefaultProperties(defaultFallback = "paymentInfoTimeOutOrException")
public class OrderFeignHystrixPayMentController {
    @Resource
    private IPayMentHystrixService iPayMentHystrixService;
    //@HystrixCommand(fallbackMethod = "paymentInfoTimeOut", commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "5500")})
    @GetMapping("/payment/hystrix/ok/{id}")
    public String paymentInfo_OK(@PathVariable("id") Integer id){
        return iPayMentHystrixService.paymentInfo_OK(id);
    }

    //@HystrixCommand(fallbackMethod = "paymentInfoTimeOut", commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "55000")})
   // @HystrixCommand(commandProperties = {@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "55000")})
    @HystrixCommand
    @GetMapping("/payment/hystrix/timeout/{id}")
    public String paymentInfo_TimeOut(@PathVariable("id") Integer id){
        System.out.println(id+"............................");
        return iPayMentHystrixService.paymentInfo_TimeOut(id);
    }
    public String paymentInfoTimeOut(@PathVariable("id") Integer id){
        return "当前自己的服务超时，请稍后重试！";
    }
    public String paymentInfoTimeOutOrException(){
        return "调用服务超时，请稍后重试！";
    }
}
