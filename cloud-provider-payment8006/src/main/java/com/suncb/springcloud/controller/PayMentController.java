package com.suncb.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "payment")
public class PayMentController {

    @Value("${server.port}")
    private String port;

    @GetMapping(value = "/consul")
    public String queryPayMent(){
        return "springcloud with consul: "+port+"\t"+ UUID.randomUUID().toString();
    }
}
