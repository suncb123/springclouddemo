package com.suncb.springcloud.controller;
import com.suncb.springcloud.entity.*;
import com.suncb.springcloud.lb.LoadBalancer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "customer")
public class OrderController {
  //  public static final String PAYMENT_URL = "http://localhost:8001";
    public static final String PAYMENT_URL = "http://CLOUD-PAYMENT-SERVICE";
    @Autowired
    private RestTemplate restTemplate;

    // ====================> zipkin+sleuth
    @GetMapping("/payment/zipkin")
    public String paymentZipkin()
    {
        String result = restTemplate.getForObject("http://localhost:8001"+"/payMentController/payment/zipkin/", String.class);
        return result;
    }

    @GetMapping(value = "/queryPayMent/{id}")
    public CommonBoy queryPayMent(@PathVariable("id") Integer id){
        ResponseEntity<CommonBoy> payMentEntity = restTemplate.getForEntity(PAYMENT_URL + "/payMentController/queryPayMent/" + id,CommonBoy.class);
        System.out.println("123");
        return payMentEntity.getBody();
    }

    @Autowired
    private LoadBalancer loadBalancer;
    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping(value = "/payment/lb")
    public String getPaymentLB(){
        List<ServiceInstance> serviceInstances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        if(null == serviceInstances || serviceInstances.size() == 0){
            return null;
        }
        ServiceInstance instance = loadBalancer.instances(serviceInstances);
        URI uri = instance.getUri();
        return restTemplate.getForObject(uri + "/payMentController/payment/lb",String.class);
    }

}
