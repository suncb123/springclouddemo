package com.suncb.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AplicationContextConfig {
    @Bean
    //@LoadBalanced
    //负载均衡 轮询
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    /*@Bean
    public RestTemplate restTemplate() {
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectionRequestTimeout(1800000);
        httpRequestFactory.setConnectTimeout(1800000);
        httpRequestFactory.setReadTimeout(1800000);
        RestTemplate restTemplate = new RestTemplate(httpRequestFactory);
        StringHttpMessageConverter t = new StringHttpMessageConverter();
        //设置为false就可以修改header中的accept-charset属性
        t.setWriteAcceptCharset(false);
        //RestTemplate的默认构造方法初始化的StringHttpMessageConverter的默认字符集是ISO-8859-1，所以导致RestTemplate请求的响应内容会出现中文乱码
        t.setDefaultCharset(StandardCharsets.UTF_8);
        restTemplate.getMessageConverters().add(0,t);
        return restTemplate;
    }*/
}

