package com.suncb.springcloud.controller.myhandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.suncb.springcloud.entity.CommonBoy;

public class CustomerBlockHandler
{
    public static CommonBoy handleException(BlockException exception){
        return new CommonBoy(2020,"自定义的限流处理信息......CustomerBlockHandler");
    }
}
