package com.suncb.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.suncb.springcloud.controller.myhandler.CustomerBlockHandler;
import com.suncb.springcloud.entity.CommonBoy;
import com.suncb.springcloud.entity.PayMentEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RateLimitController
{
    @GetMapping("/byResource")
    @SentinelResource(value = "byResource",blockHandler = "handleException")
    public CommonBoy byResource()
    {
        return new CommonBoy(200,"按资源名称限流测试OK",new PayMentEntity(2020,"serial001"));
    }
    public CommonBoy handleException(BlockException exception)
    {
        return new CommonBoy(444,exception.getClass().getCanonicalName()+"\t 服务不可用");
    }
    @GetMapping("/rateLimit/customerBlockHandler")
    @SentinelResource(value = "customerBlockHandler",
            blockHandlerClass = CustomerBlockHandler.class, blockHandler = "handleException")
    public CommonBoy customerBlockHandler()
    {
        return new CommonBoy(200,"按客户自定义限流处理逻辑");
    }

    @GetMapping("/rateLimit/byUrl")
    @SentinelResource(value = "byUrl1")
    public CommonBoy byUrl()
    {
        return new CommonBoy(200,"按url限流测试OK",new PayMentEntity(2020,"serial002"));
    }

}
