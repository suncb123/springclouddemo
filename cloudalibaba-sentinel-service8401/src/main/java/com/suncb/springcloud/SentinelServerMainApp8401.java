package com.suncb.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SentinelServerMainApp8401 {
    public static void main(String[] args) {
        SpringApplication.run(SentinelServerMainApp8401.class,args);
    }
}
