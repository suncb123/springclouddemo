package com.suncb.springcloud.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"com.suncb.springcloud.dao"})
public class MyBatisConfig {
}
