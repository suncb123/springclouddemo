package com.suncb.springcloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.ZonedDateTime;

@Configuration
public class GateWayConfig {
    @Bean
    public RouteLocator routeLocatorOne(RouteLocatorBuilder routeLocatorBuilder){
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        routes.route("routeOne", r -> r.path("/guoji").uri("https://baidu.com")).build();
        return routes.build();
    }
    /*@Bean
    public RouteLocator routeLocatorTwo(RouteLocatorBuilder routeLocatorBuilder){
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();
        routes.route("routeOne", r -> r.after(ZonedDateTime.now()).uri("")).build();
        return routes.build();
    }*/
}
