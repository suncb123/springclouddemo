package com.suncb.springcloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "customer")
public class OrderController {
    private static final String PAYMENT_URL = "http://consul-provider-payment";
    // private static final String PAYMENT_URL = "http://localhost:8004";
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping(value = "/payment")
    public String paymentInfo(){
        String result = restTemplate.getForObject(PAYMENT_URL + "/payment/consul",String.class);
        System.out.println("consul消费者:" + result);
        return result;
    }
}
