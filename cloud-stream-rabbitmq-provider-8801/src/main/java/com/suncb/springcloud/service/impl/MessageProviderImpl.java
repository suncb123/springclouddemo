package com.suncb.springcloud.service.impl;

import com.suncb.springcloud.service.IMessageProvider;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;

import javax.annotation.Resource;
import java.util.UUID;

/**
 * Description // 可以理解为是一个消息的发送管道的定义
 *
 * @Author suncb
 * @Date 2021/7/22 15:05
 */
@EnableBinding(Source.class)
public class MessageProviderImpl implements IMessageProvider {

    //消息的发送管道
    @Resource
    private MessageChannel output;

    @Override
    public String send() {
        //序号
        String serial = UUID.randomUUID().toString();
        this.output.send(MessageBuilder.withPayload(serial).build());
        System.out.println("serial:" + serial);
        return null;
    }
}
