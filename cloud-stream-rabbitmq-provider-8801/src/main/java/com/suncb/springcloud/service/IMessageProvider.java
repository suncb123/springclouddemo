package com.suncb.springcloud.service;

public interface IMessageProvider {
    public String send();
}
