package com.suncb.springcloud.service;

import com.suncb.springcloud.entity.CommonBoy;
import com.suncb.springcloud.entity.PayMentEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface IPaymentFeignService {
    @GetMapping(value = "/payMentController/queryPayMent/{id}")
    CommonBoy<PayMentEntity> getPaymentById(@PathVariable("id") Integer id);

    @GetMapping(value = "/payMentController/payment/timeout")
    String getPaymentTimeOuts();

}
