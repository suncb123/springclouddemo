package com.suncb.springcloud.controller;

import com.suncb.springcloud.entity.CommonBoy;
import com.suncb.springcloud.entity.PayMentEntity;
import com.suncb.springcloud.service.IPaymentFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "customer")
public class OrderFeignController {

    @Resource
    private IPaymentFeignService iPaymentFeignService;

    @GetMapping(value = "/getPayment/{id}")
    public CommonBoy<PayMentEntity> getPaymentById(@PathVariable("id") Integer id){
        return iPaymentFeignService.getPaymentById(id);
    }
    @GetMapping(value = "/getPayment/timeout")
    public String getPaymentTimeOut(){
        return iPaymentFeignService.getPaymentTimeOuts();
    }
}
