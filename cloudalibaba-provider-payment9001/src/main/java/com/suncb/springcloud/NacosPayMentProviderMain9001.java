package com.suncb.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class NacosPayMentProviderMain9001 {
    public static void main(String[] args) {
        SpringApplication.run(NacosPayMentProviderMain9001.class,args);
    }
}
