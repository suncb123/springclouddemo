package com.suncb.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.suncb.springcloud.entity.CommonBoy;
import com.suncb.springcloud.entity.PayMentEntity;
import com.suncb.springcloud.service.PaymentService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

@RestController
public class CircleBreakerController {
    @Value("service-url.nacos-user-service")
    private String serverUrl;

    @Resource
    private RestTemplate restTemplate;

    @RequestMapping("/consumer/fallback/{id}")
    @SentinelResource(value = "fallback", fallback = "handlerFallback", blockHandler = "blockHandler")
   // @SentinelResource(value = "fallback", fallback = "handlerFallback", blockHandler = "blockHandler", exceptionsToIgnore = {IllegalArgumentException.class})
    public CommonBoy<PayMentEntity> fallback(@PathVariable Long id)
    {
        CommonBoy<PayMentEntity> result = restTemplate.getForObject(serverUrl + "/paymentSQL/"+id, CommonBoy.class,id);

        if (id == 4) {
            throw new IllegalArgumentException ("IllegalArgumentException,非法参数异常....");
        }else if (result.getData() == null) {
            throw new NullPointerException ("NullPointerException,该ID没有对应记录,空指针异常");
        }

        return result;
    }


    //==================OpenFeign
    @Resource
    private PaymentService paymentService;

    @GetMapping(value = "/consumer/openfeign/{id}")
    public CommonBoy<PayMentEntity> paymentSQL(@PathVariable("id") Integer id)
    {
        if(id == 4)
        {
            throw new RuntimeException("没有该id");
        }
        return paymentService.paymentSQL(id);
    }


    public CommonBoy handlerFallback(@PathVariable Integer id,Throwable e) {
        PayMentEntity payment = new PayMentEntity(id,"null");
        return new CommonBoy<>(444,"兜底异常handlerFallback,exception内容  "+e.getMessage(),payment);
    }
    public CommonBoy blockHandler(@PathVariable Integer id, BlockException blockException) {
        PayMentEntity payment = new PayMentEntity(id,"null");
        return new CommonBoy<>(445,"blockHandler-sentinel限流,无此流水: blockException  "+blockException.getMessage(),payment);
    }


}
