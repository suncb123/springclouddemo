package com.suncb.springcloud.service;

import com.suncb.springcloud.entity.CommonBoy;
import com.suncb.springcloud.entity.PayMentEntity;
import org.springframework.stereotype.Component;

@Component
public class PaymentFallbackService implements PaymentService
{
    @Override
    public CommonBoy<PayMentEntity> paymentSQL(Integer id)
    {
        return new CommonBoy<>(444,"服务降级返回,没有该流水信息",new PayMentEntity(id, "errorSerial......"));
    }
}
