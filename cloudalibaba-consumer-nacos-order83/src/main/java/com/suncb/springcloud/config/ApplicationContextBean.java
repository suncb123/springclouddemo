package com.suncb.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;


@Configuration
public class ApplicationContextBean {
    @Bean
    @LoadBalanced
    public static RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
