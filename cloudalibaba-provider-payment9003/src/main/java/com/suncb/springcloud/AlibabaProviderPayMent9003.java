package com.suncb.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class AlibabaProviderPayMent9003 {
    public static void main(String[] args) {
        SpringApplication.run(AlibabaProviderPayMent9003.class,args);
    }
}
