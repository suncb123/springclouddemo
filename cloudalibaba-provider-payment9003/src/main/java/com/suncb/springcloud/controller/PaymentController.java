package com.suncb.springcloud.controller;

import com.suncb.springcloud.entity.CommonBoy;
import com.suncb.springcloud.entity.PayMentEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class PaymentController {
    @Value("${server.port}")
    private String serverPort;

    public static HashMap<Long, PayMentEntity> hashMap = new HashMap<>();
    static
    {
        hashMap.put(1L,new PayMentEntity(1,"28a8c1e3bc2742d8848569891fb42181"));
        hashMap.put(2L,new PayMentEntity(2,"bba8c1e3bc2742d8848569891ac32182"));
        hashMap.put(3L,new PayMentEntity(3,"6ua8c1e3bc2742d8848569891xt92183"));
    }

    @GetMapping(value = "/paymentSQL/{id}")
    public CommonBoy<PayMentEntity> paymentSQL(@PathVariable("id") Long id)
    {
        PayMentEntity payment = hashMap.get(id);
        CommonBoy<PayMentEntity> result = new CommonBoy(200,"from mysql,serverPort:  "+serverPort,payment);
        return result;
    }

}
